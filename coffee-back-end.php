<?php
require 'database.php';
if(!empty($_POST["user"])
   &&!empty($_POST["coffee"])
   && !empty($_POST["userRating"])
   && !empty($_POST["comments"])){
   
   $user = $_POST["user"];
   $coffee = $_POST["coffee"];
   $userRating = intval($_POST["userRating"]);
   $comments = $_POST["comments"];
   
   $stmt = $mysqli->prepare("INSERT INTO coffee (name) VALUES (?)");
   if(!$stmt){
      printf("Query Prep Failed: %s\n", $mysqli->error);
      exit;
   }
   $stmt->bind_param('s',$coffee);
   $stmt->execute();
   $cId = $stmt->insert_id;
   $stmt->close();
   
   $stmt2 = $mysqli->prepare("INSERT INTO ratings (coffeeId, user, RATING, comment) VALUES (?,?,?,?)");
   if(!$stmt2){
      printf("Query Prep Failed: %s\n", $mysqli->error);
      exit;
   }
   $stmt2->bind_param('isis',$cId,$user,$userRating,$comments);
   $stmt2->execute();
   $stmt2->close();
   header("Location: coffee-main.html");
}
else{
   echo "Please fill out all inputs";
   exit;
}
?>