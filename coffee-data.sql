create database coffee;
use coffee;
create table coffee(
    -> id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    -> name VARCHAR(50) NOT NULL,
    -> PRIMARY KEY(id)
    -> ) engine = INNODB DEFAULT character SET = utf8 COLLATE = utf8_general_ci;
create table ratings(
    -> coffeeId INT UNSIGNED NOT NULL,
    -> user VARCHAR(50) NOT NULL,
    -> RATING DECIMAL(1,1) NOT NULL,
    -> PRIMARY KEY (coffeeId),
    -> FOREIGN key (coffeeId) REFERENCES coffee(id)
    -> ) engine = INNODB DEFAULT character SET = utf8 COLLATE = utf8_general_ci;
ALTER TABLE ratings add comment TINYTEXT NOT NULL;
GRANT SELECT,INSERT,UPDATE,DELETE on coffee.* to module@'localhost';