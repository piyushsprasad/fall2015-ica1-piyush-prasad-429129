<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>Coffee Results</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
h1{
   text-align: center;
}
</style>
</head>
<body><div id="main">
   <h1>Coffee Ratings</h1>
   <?php
      require 'database.php';
      if(!empty($_POST["coffee"])){
         $coffee = $_POST["coffee"];
         $idstmt = $mysqli->prepare("SELECT id from coffee where name = ?");
         if(!$idstmt){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
         }
         $idstmt->bind_param('s',$coffee);
         $idstmt->execute();
         $idstmt->bind_result($id);
         $idstmt->fetch();
         $idstmt->close();
         
         $stmt = $mysqli->prepare("SELECT avg(RATING) from ratings where coffeeId =?");
         if(!$stmt){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
         }
         $stmt->bind_param('s',$id);
         $stmt->execute();
         $stmt->bind_result($avgRating);
         $stmt->fetch();
         $stmt->close();
         echo "Name: ".htmlentities($coffee)."<br>";
         echo "Average Rating (out of f): ".htmlentities($avgRating)."<br>";
         $stmt2 = $mysqli->prepare("SELECT user, comment from ratings where coffeeId =?");
         if(!$stmt2){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
         }
         $stmt2->bind_param('s',$id);
         $stmt2->execute();
         $stmt2->bind_result($user,$comment);
         while($stmt2->fetch()){
            echo "".htmlentities($user).":    '".htmlentities($comment)."'";
         }
         $stmt2->close();
         echo "<br>";
         $link = "http://ec2-52-88-200-0.us-west-2.compute.amazonaws.com/~piyushhp/ica1/coffee-main.html";
         echo "<a href='".$link."'>Submit a New Rating</a>";
      }
   ?>
 
</div></body>
</html>